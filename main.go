package main

import (
	"fmt"
)

const (
	// @Translation
	helloWorldEn = "Hello, World!"
)

func main() {
	fmt.Println(helloWorldEn)
}
